module MyValidation

  def self.included(base)
    base.extend ClassMethods
  end

  def validate!
    begin
      make_validations
      print 'Validation complete, entity is valid.'
    rescue RuntimeError => e
      print(e.message)
    end
  end

  def valid?
    begin
      make_validations
      return true
    rescue RuntimeError
      return false
    end
  end


  private
  
  def make_validations
      validations = self.class.current_validations
      validations.each{|v| make_validation(*v)} if validations
  end

  def make_validation(validated_field, options)
    validated_value = self.send(validated_field)
    options.each do |validation_type, validation_argument|
      valid, message = case validation_type
                         when :presence
                           presence_validation(validated_value, validation_argument)
                         when :format
                           format_validation(validated_value, validation_argument)
                         when :type
                           type_validation(validated_value, validation_argument)
                         else
                           p("Undefined validation type, validation was skipped.")  
      end  
      raise("Validation on #{validated_field} failed! #{message}") unless valid
    end
  end

  def presence_validation(value, presence_neaded)
    presence = !value.nil? && value != ""
    [presence == presence_neaded, "'#{value}' hasn't pass presece validation."]
  end

  def format_validation(value, argument)
    regex_argument = Regexp.new(argument)
    [!!value.to_s.match(regex_argument), "'#{value}' is not suitable for regexp: #{argument}."]
  end

  def type_validation(value, argument)
    [value.class == argument, "'#{value}' is not #{argument} type."]
  end


  module ClassMethods

    @@my_validations = {}

    def current_validations
      @@my_validations[self.to_s]
    end

    def delete_my_validation
      @@my_validations[self.to_s] = []
    end

    def validate(attribute_name, options)
      if attribute_name_correct?(attribute_name)
        add_validation(attribute_name, options)
      else
        print("Incorrect attribute name, validation can not take place")
      end
    end

    def attribute_name_correct?(attribute_name)
      self.instance_methods.include?(attribute_name) && self.instance_methods.include?("#{attribute_name}=".to_sym)
    end

    def add_validation(attribute_name, options)
      @@my_validations[self.to_s] ||= []
      @@my_validations[self.to_s] << [attribute_name, options]
    end

  end
    
end
