require_relative "my_validation.rb"

class User
  include MyValidation
  attr_accessor :age, :name, :city
end

User.validate(:age, type: Integer)
User.validate(:city, presence: true)
User.validate(:name, format: "^[a-z]")
# User.validate(:phone, presence: true) => Incorrect attribute name, validation can not take place

u1 = User.new
u1.age = "9"
# u1.valid? => false
# u1.validate! => Validation on age failed! '9' is not Integer type.

u2 = User.new
u2.age = 9
# u2.valid?# => false
# u2.validate!# => Validation on city failed! '' hasn't pass presece validation.

u3 = User.new
u3.age = 9
u3.city = "Skole"
u3.name = "Oleh"
# u3.valid?# => false
# u3.validate!# => Validation on name failed! 'Oleh' is not suitable for regexp: ^[a-z].

u4 = User.new
u4.age = 9
u4.city = "Skole"
u4.name = "oleh"
#p u4.valid?# => true
#u4.validate!# => Validation complete, entity is valid.
